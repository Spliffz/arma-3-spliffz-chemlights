/*
	Spliffz Chemlights
	init.sqf
	2014 - Spliffz <theSpliffz@gmail.com>
*/

if(isDedicated) exitWith {};

/* The Code */
waitUntil { player == player };


// ## Defines
#define SHIFTL 42
#define SHIFTR 54
#define CTRLL 29
#define ALTL 56

[] execVM "\spliffz_chemlights\chemlights\chemlights.sqf";


// EOF