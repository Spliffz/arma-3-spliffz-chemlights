/*
	Spliffz Chemlights
	\spliffz_chemlights\chemlights\chemlights.sqf
	Spliffz - 2014
	
	// attach/detach chemlight/ir-strobe to person
	
	v1.0
*/

#include "\spliffz_chemlights\config\keys.sqf"
#include "\a3\editor_f\Data\Scripts\dikCodes.h"


GC_chemlights_attach = {
	private ["_chemlight", "_chemlight_to_attach", "_chemlightcolor", "_split", "_type", "_color"];
	//diag_log format ["gc_chemlights_attach: %1", _this];
	_split = [(_this select 0), ";"] call BIS_fnc_splitString;
	_type = _split select 0;
	_color = _split select 1;
	
	if(_type == "chemlight") then {
		_chemlightColor = _color;
		
		//diag_log format ["_chemlightcolor: %1", _chemlightcolor];
		
		_chemlight = format ["Chemlight_%1", _chemlightcolor];
		//hint _chemlight;
		
		if (_chemlight in (magazines player)) then { 
			player removeMagazine _chemlight; 
		};

		_chemlight_to_attach = _chemlight createVehicle [0,0,0];
		_chemlight_to_attach attachTo [player, [-0.02, -0.05, 0.045], "LeftShoulder"];
		
		player setVariable ["GC_chemlight_attached", true, false];
		player setVariable ["GC_chemlight_object", _chemlight_to_attach, false];
	};
	
	if(_type == "irstrobe") then {
		_chemlight = "NVG_TargetC";
		
		
		// future: replace this hardcoded stuff with dynamic found ir grenade class/side/type
		switch (side player) do {
			case "west": {
				player removeMagazine "B_IR_Grenade";
			};
			
			case "east": {
				player removeMagazine "O_IR_Grenade";
			};
			
			case "guer": {
				player removeMagazine "I_IR_Grenade";
			};
		};
		
		_chemlight_to_attach = _chemlight createVehicle [0,0,0];
		_chemlight_to_attach attachTo [player, [0,0,0], "LeftShoulder"];
		
		player setVariable ["GC_chemlight_attached", true, false];
		player setVariable ["GC_chemlight_object", _chemlight_to_attach, false];
	
	};
	
};


GC_chemlights_detach = {
	private ["_chemlight", "_chemlight_attached", "_pos"];
	_chemlight = player getVariable ["GC_chemlight_attached", false];
	if!(_chemlight) exitWith {};
	
	_chemlight_attached = player getVariable ["GC_chemlight_object", false];
	_pos = getPosATL player;
	detach _chemlight_attached;
	_chemlight_attached setPosATL _pos;
	
	player setVariable ["GC_chemlight_attached", false, false];
	player setVariable ["GC_chemlight_object", nil, false];
};


GC_chemlights_irstrobe_subMenu = {
	private ["_subMenuHeader", "_subMenuInsides", "_subMenu"];
	_subMenuHeader = ["chemlights_secondary", "IR-Strobe Menu", "popup", "", false];
	_subMenuInsides = [];
	
	// submenu item
	private ["_title", "_act", "_action", "_item"];
	_title = format ["Attach IR-Strobe"];
	_action = format ["irstrobe;%1; call GC_chemlights_attach", 0];
	//diag_log format ["_action: %1", _action];

	_item =
	[
		_title,
		_action,
		"",
		"Attach ir-strobe to shoulder",
		"",
		-1,
		true,
		true
	];
	_subMenuInsides set [count _subMenuInsides, _item];
	
	_subMenu = 
	[
		_subMenuHeader,
		_subMenuInsides
	];
	
	//diag_log format ["submenu: %1", _subMenu];
	_subMenu
};


// submenu for each chemlight
GC_chemlights_subMenu = {
	private ["_mags", "_chemcolors", "_subMenuHeader", "_subMenuInsides", "_subMenu"];
	_mags = magazines player;
	_chemcolors = [];
	{
		// sort by color
		switch (tolower(_x)) do {
			case "chemlight_green": {
				if!("green" in _chemcolors) then {
					_chemcolors set [count _chemcolors, "green"];
				};
			};
			case "chemlight_blue": {
				if!("blue" in _chemcolors) then {
					_chemcolors set [count _chemcolors, "blue"];
				};
			};
			case "chemlight_red": {
				if!("red" in _chemcolors) then {
					_chemcolors set [count _chemcolors, "red"];
				};
			};
			case "chemlight_yellow": {
				if!("yellow" in _chemcolors) then {
					_chemcolors set [count _chemcolors, "yellow"];
				};
			};
		};
	} foreach _mags;
	
	//diag_log format ["chemcolors: %1", _chemcolors];
	
	_subMenuHeader = ["chemlights_secondary", "Chemlights Menu", "popup", "", false];
	_subMenuInsides = [];
	
	
	{
		// submenu item
		private ["_title", "_act", "_action", "_item"];
		_title = format ["Attach %1 Chemlight", _x];
		_action = format ["chemlight;%1; call GC_chemlights_attach", _x];
		//diag_log format ["_action: %1", _action];

		_item =
		[
			_title,
			_action,
			"",
			"Attach chemlight to shoulder",
			"",
			-1,
			true,
			true
		];
		_subMenuInsides set [count _subMenuInsides, _item];
	} foreach _chemcolors;
	
	_subMenu = 
	[
		_subMenuHeader,
		_subMenuInsides
	];
	
	//diag_log format ["submenu: %1", _subMenu];
	_subMenu
};


GC_chemlights_menu = {
	private ["_invHasChemlight", "_hasChemlightAttached", "_innerMenu"];
	_invHasChemlight = false;
	_hasChemlightAttached = player getVariable ["GC_chemlight_attached", false];
	
	//diag_log format ["_hasChemlightAttached: %1", _hasChemlightAttached];
	
	// chemlights inventory check
	if(("Chemlight_green" in (magazines player)) || ("Chemlight_red" in (magazines player)) || ("Chemlight_yellow" in (magazines player)) || ("Chemlight_blue" in (magazines player))) then {
		_invHasChemlight = true;
	};	
	
	// ir-strobe inventory check
	_invHasIRStrobe = false;
	//_hasIRStrobeAttached = player getVariable ["GC_chemlight_irstrobe_attached", false];
	_hasIRStrobeAttached = player getVariable ["GC_chemlight_attached", false];
	if(("B_IR_Grenade" in (magazines player)) || ("O_IR_Grenade" in (magazines player)) || ("I_IR_Grenade" in (magazines player))) then {
		_invHasIRStrobe = true;
	};

	
	_menu = "";
	_menuDlg = "";
	_innerMenu = [];
	
	// checmlights
	if(_invHasChemlight && !_hasChemlightAttached) then {
		private ["_subMenu", "_action", "_icon", "_menuDlg", "_innerMenuChemlights", "_menu"];
		_subMenu = "_this call GC_chemlights_subMenu";
		_action = "";
		_icon = "";
		_menuDlg = ["chemlights_main", "Chemlights Options", "popup", "", false];
		_innerMenuChemlights = 
		[
			"Attach Chemlight",
			_action,
			_icon,
			"Attach chemlight to shoulder",
			_subMenu, // submenu
			-1,
			true,
			true // menu visibility condition
		];
		_innerMenu set [count _innerMenu, _innerMenuChemlights];
		
		
		
		// IR-Strobes
		if(_invHasIRStrobe && !_hasIRStrobeAttached) then {
			private ["_subMenu", "_action", "_icon", "_innerMenuIRStrobes", "_menu"];
			_subMenu = "_this call GC_chemlights_irstrobe_subMenu";
			_action = "";
			_icon = "";
			_innerMenuIRStrobes = 
			[
				"Attach IR-Strobe",
				_action,
				_icon,
				"Attach IR-Strobe to shoulder",
				_subMenu, // submenu
				-1,
				true,
				true // menu visibility condition
			];
			_innerMenu set [count _innerMenu, _innerMenuIRStrobes];
		};
		
		
		
		
		// display menu
		_menu = [
			_menuDlg,
			_innerMenu
		];
		
		// retval
		_menu
		
	} else { 
	
		// remove chemlight
		private ["_subMenu", "_action", "_icon", "_menuDlg", "_innerMenuChemlights", "_menu"];
		_subMenu = "";
		_action = "_this call GC_chemlights_detach";
		_icon = "";
		_menuDlg = ["chemlights_main", "Chemlights Options", "popup", "", false];
		_innerMenuChemlights = 
		[
			"Detach Chemlight",
			_action,
			_icon,
			"Detach chemlight from shoulder",
			_subMenu, // submenu
			-1,
			true,
			true // menu visibility condition
		];
		_innerMenu set [count _innerMenu, _innerMenuChemlights];
		
		
		// remove ir-strobe
		if(!_invHasIRStrobe && _hasIRStrobeAttached) then {
			private ["_subMenu", "_action", "_icon", "_innerMenuIRStrobes", "_menu"];
			_subMenu = "";
			_action = "_this call GC_chemlights_detach";
			_icon = "";
			_innerMenuIRStrobes = 
			[
				"Detach IR-Strobe",
				_action,
				_icon,
				"Detach IR-Strobe from shoulder",
				_subMenu, // submenu
				-1,
				true,
				true // menu visibility condition
			];
			_innerMenu set [count _innerMenu, _innerMenuIRStrobes];
		};

		
		// display menu
		_menu = [
			_menuDlg,
			_innerMenu
		];
		
		// retval
		_menu
		
	};
};


/* ~ The Code ~ */
[player] spawn {

	["player", [[spliffz_chemlights_menu_key, [spliffz_chemlights_menu_key_shift == 1, spliffz_chemlights_menu_key_ctrl == 1, spliffz_chemlights_menu_key_alt == 1]]], -3, '_this call GC_chemlights_menu'] call CBA_fnc_flexiMenu_Add;
	
	while {true} do {
		if(player == vehicle player) then {
			call GC_chemlights_detach;
		};
	};
	
};


// EOF