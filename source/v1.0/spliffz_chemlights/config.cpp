class CfgPatches
{
	class spliffz_chemlights
	{
		units[] = { };
		weapons[] = { };
		requiredAddons[] = {"CBA_Extended_EventHandlers"};
		version = "1.0";
		versionStr = "1.0";
		versionDesc= "Spliffz Chemlights";
		versionAr[] = {2014,2,15};
		author[] = {"Spliffz"};
	};
};

class Extended_PostInit_EventHandlers 
{
	class spliffz_chemlights
	{
		clientInit = "execVM '\spliffz_chemlights\init.sqf';";
	};
};


#include "\userconfig\spliffz_chemlights\keys.hpp"


// EOF