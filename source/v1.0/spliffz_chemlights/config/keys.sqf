/*
	Spliffz Chemlights
	\spliffz_chemlights\config\keys.sqf
	Spliffz - 2014
*/

// Keys
spliffz_chemlights_menu_key = getNumber (configFile >> "spliffz_chemlights" >> "chemlights_menu" >> "key");
spliffz_chemlights_menu_key_shift = getNumber (configFile >> "spliffz_chemlights" >> "chemlights_menu" >> "shift");
spliffz_chemlights_menu_key_ctrl = getNumber (configFile >> "spliffz_chemlights" >> "chemlights_menu" >> "ctrl");
spliffz_chemlights_menu_key_alt = getNumber (configFile >> "spliffz_chemlights" >> "chemlights_menu" >> "alt");


// EOF