Spliffz Chemlights
v1.0
2014 Spliffz <thespliffz@gmail.com>


Attach A Chemlight/IR-Strobe to your person


[INFORMATION]
This little addon gives you the option to attach a chemlight or IR-Strobe to your person.
The current interaction (chemlight menu) keys are "shift+left windows", but these are configurable in the userconfig file in arma3\userconfig\spliffz_chemlights\keys.hpp.


[HOW IT WORKS]
Put a chemlight or ir-strobe in your inventory.
Press the chemlights menu key.
Click on "Attach Chemlight" or "Attach IR-Strobe"
If you have more then 1 color chemlight on you, you will have different options.
Once you wear a chemlight or ir-strobe, that item will be deleted from your inventory (you are using it after all.) And you can use 1 item at a time. Or a chemlight, Or a IR-Strobe.
If you come at a point when you don't need it anymore, or need to put on a new one, press the menu key again and click "Detach chemlight".


[REQUIREMENTS]
Arma 3
Community Base Addons (CBA A3)


[INSTALLATION]
- Unpack the .zip and copy the @spliffz_chemlights folder to your Arma 3 game directory.
- If you already have @CBA_A3 you don't have to copy this.
- Copy the userconfig folder from within the @spliffz_chemlights mod folder to your Arma 3 game directory.
- If you want to reconfigure the keys just edit the keys.hpp file in the arma3\userconfig\spliffz_chemlights\ folder.


[KNOWN ISSUES]
- Sometimes the chemlight keeps bouncing on the ground when detached. Gives a cool side effect though.


[THANKS]
Thanks to the guy that made the CBA UI wiki page. It's not much but it was definitely helpfull! 


[DISCLAIMER]
I (Spliffz) take no responsibility for anything that might happen with your game, computer, life or anything else when using this (my) mod(s).
By acknowledging this, you are now allowed to use this mod and to modify the code, on the condition that you share your changes/the code with me/the community, so that we can all benefit from it.
And if you decide to use this script or parts from it in other scripts/mods or addons then be so kind to put my name in there too.

